import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { UserService } from '../../../service/user.service';
import { user } from '../../../models/User';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public form: FormGroup;
	public submitted: boolean = false;
  public response = {
    message: "",
  }

	constructor(
    private formBuilder: FormBuilder, 
    private userService: UserService,
    private router: Router
  ) 
  {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.maxLength(20)]],
      password: ['', [Validators.required, Validators.maxLength(20)]],
    });
      
	 }

  ngOnInit(): void {}

  public onSubmit(): void {
    this.submitted = true;
    if (this.form.valid) {
      const userLogin: user = {
        email: this.form.get('email').value,
        password: this.form.get('password').value,
      };
      console.log(userLogin);
      const response = this.userService.registerUser(userLogin);
     response.then(data => {
       console.log(data.message);
       data.message ? this.router.navigate(['/']) : this.router.navigate(['/api']);
       this.form.reset();
      this.submitted = false;
     })
      
    }
  }
}