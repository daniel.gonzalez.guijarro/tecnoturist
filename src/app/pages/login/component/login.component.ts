import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { UserService } from '../../../service/user.service';
import { user } from '../../../models/User';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public form: FormGroup;
	public submitted: boolean = false;
  public response = {
    message: "",
  }

	constructor(
    private formBuilder: FormBuilder, 
    private userService: UserService,
    private router: Router
  ) 
  {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.maxLength(20)]],
      password: ['', [Validators.required, Validators.maxLength(20)]],
    });
      
	 }

  ngOnInit(): void {}

  public onSubmit(): void {
    this.submitted = true;
    if (this.form.valid) { //validacion del formulario.
      const userLogin: user = {
        email: this.form.get('email').value,
        password: this.form.get('password').value,
      };
      const response = this.userService.loginUser(userLogin);  //peticion fetch a traves del servicio.
      response.then(data => {
       data.message ? this.router.navigate(['/']) : this.router.navigate(['/api']); // si la peticion recibe un message. es que no existe el user y nos redigrige al HOME.
       this.form.reset();
      this.submitted = false;
     })
      
    }
  }
 
}
