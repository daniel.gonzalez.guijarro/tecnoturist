import { Component, OnInit } from '@angular/core';
import { AmadeusService } from 'src/app/service/amadeus.service';
import { user } from '../../../models/User';

@Component({
  selector: 'app-api',
  templateUrl: './api.component.html',
  styleUrls: ['./api.component.scss']
})
export class ApiComponent implements OnInit {
  prueba: user;
  characterList: any[] = [];
  public hotels:any = null
  public hotelList = [];
  constructor(private amadeusService: AmadeusService) {
  }

  ngOnInit(): void {
    
   const hola =  this.amadeusService.getHotels().subscribe(((result:any[]) => {
    this.hotels = result;
    this.hotelList = this.hotels.data;
    console.log(this.hotelList)
    
    // const formattedResults = results.map(({ id, name, image }) => ({
    //   id,
    //   name,
    //   image,
    // }));
    // this.characterList = formattedResults;
  }));
  //  hola.then(data => {
  //    this.hotelList = data.data;
  //    
  //    console.log("hotel: "+ this.hotelList)
  //   })
  }
}
