import { API_URL } from '../utils/key';
import { Injectable } from '@angular/core';
import { user } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url = API_URL.url;
 
  constructor() {}

  public async registerUser(user: user)
  {
      const request = await fetch("http://localhost:5000/register", {
        method: "POST",
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
        },
        credentials: 'include',
        body: JSON.stringify(user),
      })
      const response = await request.json();
      if(!request.ok) {
        return new Error(response.message);
      }
      return response;
    }
  
  public async loginUser(user: user)
  {
    
    const request = await fetch("http://localhost:5000/login", {
      method: "POST",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
      credentials: 'include',
      body: JSON.stringify(user),
    })
    const response = await request.json();
    
    if(!request.ok) {
      return new Error(response.message);
    }
    return response;   
  }
  public logOut(){}

}
