import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})

export class AmadeusService {
  
  constructor(private http: HttpClient) {}  
 
  public getHotels():Observable<any> //petición através de observable
  {
    return this.http.get("http://localhost:5000/hotel", { //suelo poner la url en otro archivo como constantes.
      headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*",
              },
    }).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected!');
        } else {}
           return response; //devuelve el observable de la peticion hecha en el back
        }))
  }
}
    
   
 

